# DNS Finder

This script is used to generate a subnet table that contains all of the IPs in the /24 subnet. It checks to see if they are in the ARP table of the router, if the IP responds to a ping, and if the DNS entry is correct. It then presents it in a table to easily find free IPs.

Requirements
* Python 2.4+
* fping
* net-snmp (snmpwalk)
* snmp access to the L3 router.
