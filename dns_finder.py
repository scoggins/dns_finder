#!/usr/bin/env python
# encoding: utf-8
"""
dns_finder.py v 1.0

Created by Simon Coggins on 2011-07-14.
Copyright (c) 2011 CQUniversity. All rights reserved.
"""

import sys, socket, os, os.path, cPickle
import subprocess, threading
import cgi, re, time


# Create a file /etc/site-snmp-community.txt and put in the snmp community string, make it only readable to root and the webserver.
SNMP_COMMUNITY = open("/etc/site-snmp-community.txt").read().strip() # Read the comunity string from the FS
VALID_SUBNET_RE = "^(138\.77\.[0-9]{1,3}|203\.27\.22[01]|10\.[0-9]{1,3}\.[0-9]{1,3}|192.168.[0-9]{1,3})$"
ROUTER_D_IP = "1" 
CACHE_DIR = "/tmp"
CACHE_TIMEOUT = 900


def getPingForSubnet(SubnetData):
    cmd = "/usr/sbin/fping -a -g %s.0/24" % SubnetData["subnet"]
    ping = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (sOut, sError) = ping.communicate()
    for line in sOut.split("\n"):
        if not line:
            continue  
        try:
            ip = line.split()[0]
            if ip in SubnetData["ips"]:
                SubnetData["ips"][ip]["PING"] = "YES"
        except:
            continue


def getArpEntriesFromSwitch(SubnetData):
    cmd = "snmpwalk -O n -v 1 -c %s %s.%s .1.3.6.1.2.1.4.22.1.2" % (SNMP_COMMUNITY, SubnetData["subnet"], ROUTER_D_IP)
    snmp = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (sOut, sError) = snmp.communicate()
    for line in sOut.split("\n"):
        if not line:
            continue
        try:
            (oid, equals, oidtype, value) = line.split()
            oid = oid.split(".")
            ip = ".".join(oid[-4:])
            subnet = ".".join(oid[-4:-1])
            if ip in SubnetData["ips"]:
                SubnetData["ips"][ip]["ARP"] = value
        except:
            continue

def getDNSForSubnet(SubnetData):
    for D in range(1, 254):
        ip = "%s.%s" % (SubnetData["subnet"], D)
        if ip not in SubnetData["ips"]:
            continue
        try:
            rDns = socket.gethostbyaddr(ip)[0]
            try:
                fDns = socket.gethostbyname(rDns)
            except:
                fDns = ""
        except:
            rDns = ""
            fDns = ""
                
        
        SubnetData["ips"][ip]["DNSFWD"] = fDns
        SubnetData["ips"][ip]["DNSREV"] = rDns
        

def HTMLHeader(title=""):
    print """
<html>
  <head>
    <title>%s</title>
    <style type="text/css">
/* Design 4 */
table {
    border-spacing: 0px;
    border-collapse: collapse;
}
table th {
    text-align: left;
    font-weight: normal;
    padding: 0.1em 0.5em;
    border-bottom: 2px solid #FFFFFF;
    background: #DBE2F1;
}
table td {
    text-align: right;
    border-bottom: 2px solid #FFFFFF;
    padding: 0.1em 0.5em;
}
table tr {
    background: #DBE2F1;
}

table tbody tr.free {
    background: #DBF1E2;
}

table tbody tr.error {
    background: #F1DBE2;
}

table thead th {
    background: #687EAB;
    color: #FFFFFF;
    text-align: center;
}
table th.Corner {
    text-align: left;
}
    </style>
  </head>
  <body>
""" % title

def HTMLFooter():
    print """
  </body>
</html>
"""

def displaySubnetTable(SubnetData):
    HTMLHeader(title="Subnet Results")
    print "<table>"
    print "<thead><tr><th>IP</th><th>DNS</th><th>ARP</th><th>PING</th><th>Status</th><th>Comments</th></tr></thead>"
    print "<tbody>"
    for D in range(1, 255):
        ip = "%s.%s" % (SubnetData["subnet"], D)
        comment = ""
	id = "inuse"
        if SubnetData["ips"][ip]["DNSREV"] != "" and SubnetData["ips"][ip]["DNSFWD"] != ip:
            id = "error"
            if SubnetData["ips"][ip]["DNSFWD"] != "":
                comment = "DNS Mismatch - %s resolves to %s" % (SubnetData["ips"][ip]["DNSREV"], SubnetData["ips"][ip]["DNSFWD"])
            else:
                comment = "DNS Mismatch - %s does not resolve" % (SubnetData["ips"][ip]["DNSREV"])
        if SubnetData["ips"][ip]["PING"] == "YES" and SubnetData["ips"][ip]["DNSREV"] == "":
            comment = "NO DNS Enrty!"
            id = "error"
        if SubnetData["ips"][ip]["ARP"] == "" and SubnetData["ips"][ip]["PING"] == "NO":
            id = "free"
        print "<tr class='%s'><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>" % (id, ip, SubnetData["ips"][ip]["DNSREV"], SubnetData["ips"][ip]["ARP"], SubnetData["ips"][ip]["PING"], id, comment)
    print "</tbody>"
    print "</table>"      
    HTMLFooter()

def displayForm(error=""):
    HTMLHeader(title="Enter Subnet")
    if error != "":
        error = "<span style='text-colour: red'>ERROR: %s</span>" % error
    print """
    <h1>Subnet DNS Finder</h1>
    <p>This script will take a subnet and produce a table that shows IPs that are in use, free and have DNS errors. It can take up to 2 minutes to run. </p>
    <p />
    <p>EG: 138.77.5 or 138.77.34 etc.</p>
    %s
    <form method="POST">
      Subnet: <input type=text name="subnet" /><input type=submit value=submit /><br />
    </form>""" % error
    HTMLFooter()
    

def updateCacheForSubnet(SubnetData):
    cache = { "data":SubnetData, "cachetime":time.time() }
    filename = os.path.join(CACHE_DIR, "dns-finder-%s.cache" % SubnetData["subnet"])
    try:
        cPickle.dump(cache, open(filename, "w"))
    except:
        pass

def loadCacheForSubnet(SubnetData):
    filename = os.path.join(CACHE_DIR, "dns-finder-%s.cache" % SubnetData["subnet"])
    if os.path.exists(filename):
        cache = cPickle.load(open(filename, "r"))
        if (time.time() - cache["cachetime"]) <= CACHE_TIMEOUT: 
            # Cache is good lets use it!
            SubnetData["ips"] = cache["data"]["ips"]
            return True
    
    return False
        
def main(argv=None):
    print "Content-Type: text/html"
    print 

    SubnetData = {}  
    form = cgi.FieldStorage()
    if "subnet" in form:
        subnet = form["subnet"].value
        if not re.match(VALID_SUBNET_RE, subnet):
            displayForm("Subnet %s is not a valid CQU subnet!" % subnet)
            sys.exit(0)
          
        SubnetData["subnet"] = subnet
        SubnetData["ips"] = {}
    
        for D in range(1, 255):
            SubnetData["ips"]["%s.%s" % (subnet, D)] = { "ARP":"", "DNSFWD":"", "DNSREV":"", "PING":"NO" }

        
        if not loadCacheForSubnet(SubnetData):
            arpThread = threading.Thread(target=getArpEntriesFromSwitch, args=[SubnetData, ])
            arpThread.start()
            pingThread = threading.Thread(target=getPingForSubnet, args=[SubnetData, ])
            pingThread.start()
            dnsThread = threading.Thread(target=getDNSForSubnet, args=[SubnetData, ])
            dnsThread.start()
            
            #Wait for the threads to finish.
            dnsThread.join()
            pingThread.join()
            arpThread.join()
    
            updateCacheForSubnet(SubnetData)
          
        displaySubnetTable(SubnetData)
    
    else:
      displayForm()

if __name__ == "__main__":
    sys.exit(main())
